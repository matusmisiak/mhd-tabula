import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse, parse_qs
import re, json
import io, zipfile, csv
import os, shutil
import datetime
import unicodedata
from time import sleep
from dotenv import load_dotenv

STOPS_FILE = 'stops.js'
OLD_ST_DIR = 'old_stops'

load_dotenv()
API_KEY = os.getenv('OPENDATA_API_KEY')

def lower_normalize(s):
   return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn').lower()

# Scrape stops from imhd.sk
stops = {}

print('Getting stop list from imhd.sk...')
site = requests.get('https://imhd.sk/ba/cestovne-poriadky').text
soup = BeautifulSoup(site, 'html.parser')
stoptions = soup.find(id='stopSectionNames').find_all('option')

print('Scraping stop IDs...')
for s in stoptions:
    whole_name = s.get_text().strip()
    if whole_name.split(', ')[0] == 'Bratislava':
        stop_site = requests.get(f'https://imhd.sk/ba/zastavka/{s["value"]}').text
        stop_soup = BeautifulSoup(stop_site, 'html.parser')
        for iframe in stop_soup.find_all('iframe'):
            if 'online-zastavkova-tabula' not in iframe['src']:
                continue
            stop_caption = stop_soup.find('h1', text=re.compile('Zastávka .*'))
            name = stop_caption.get_text().replace('Zastávka ', '').strip()
            stop_id = parse_qs(urlparse(iframe['src']).query)['st'][0]
            stops[stop_id] = name
            print(f'{stop_id:>4} {name}')
            break
        sleep(1)

# Pair with opendata stops
print('Getting stop positions from data.bratislava.sk...')
z = requests.get('https://www.arcgis.com/sharing/rest/content/items/aba12fd2cbac4843bc7406151bc66106/data').content
zf = zipfile.ZipFile(io.BytesIO(z), "r")
with io.TextIOWrapper(zf.open('stops.txt'), encoding='utf-8') as f:
    opendata_stops = list(csv.DictReader(f))

new_stops = {}
not_found = []
for st_id, name in stops.items():
    new_stops[st_id] = {'name': name}
    for o_stop in opendata_stops:
        if lower_normalize(name) == lower_normalize(o_stop['stop_name']):
            coords = {'lat': o_stop['stop_lat'], 'lon': o_stop['stop_lon']}
            if 'coords' in new_stops[st_id]:
                new_stops[st_id]['coords'].append(coords)
            else:
                new_stops[st_id]['coords'] = [coords]
    if 'coords' not in new_stops[st_id]:
        not_found.append((st_id, name))
if not_found:
    print('Not found:')
    for st_id, name in not_found:
        print(f'{st_id:>4} {name}')

# Export
print('Exporting stops to', STOPS_FILE)
if os.path.isfile(STOPS_FILE):
    if not os.path.isdir(OLD_ST_DIR):
        os.mkdir(OLD_ST_DIR)
    fname, ext = os.path.splitext(STOPS_FILE)
    new_name = datetime.datetime.now().strftime(f'{fname}_%Y-%m-%d{ext}')
    shutil.copyfile(STOPS_FILE, os.path.join(OLD_ST_DIR, new_name))

with open(STOPS_FILE, 'w', encoding='utf-8') as file:
    print('var stops =', json.dumps(new_stops, ensure_ascii=False, indent=2), file=file)
