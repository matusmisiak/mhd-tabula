import json
import time
import os
import requests
import unicodedata
from geopy.geocoders import Nominatim
from dotenv import load_dotenv

def list_to_coords(l):
    latsum = 0
    lonsum = 0
    for c in l:
        latsum += c['lat']
        lonsum += c['lon']
    return (latsum/len(l), lonsum/len(l))

def lower_normalize(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn').lower()

load_dotenv()
API_KEY = os.getenv('OPENDATA_API_KEY')

geolocator = Nominatim(user_agent="stop-finder")

resp = requests.get('https://opendata.bratislava.sk/api/mhd/stationstop', headers={'key': API_KEY})
stops = json.loads(resp.text)
names = ['Plniareň plynu']
stops = [st for st in stops if st['name'] in names]

new_stops = {}

for st in stops:
    if st['name'] not in new_stops:
        new_stops[st['name']] = (st['gpsLat'], st['gpsLon'])

print(len(new_stops))

geojson = []
for name, (lat, lon) in new_stops.items():
    geojson.append({
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [lon, lat]
        },
        "properties": {
            "name": name
        }
    })

with open('geojson_stops_nekvalitne.json', 'w', encoding='utf-8') as file:
    json.dump(geojson, file)

# with open('stops_excel.txt', 'w', encoding='utf-8') as excel_file:
#     for name, (lat, lon) in new_stops.items():
#         location = geolocator.reverse((lat, lon))
#         address = location.raw['address']
#         key = 'town' if 'town' in address else 'suburb' if 'suburb' in address and address['suburb'] != 'Bratislava' else ''
#         out_addr = ''
#         if key:
#             out_addr = address[key].replace('Bratislava - mestská časť ', '').strip()
#         out_dist = address['city_district'].replace('okres ', '').strip() if 'city_district' in address else ''
#         output = f"{name}\t{out_addr}\t{out_dist}\t=HYPERLINK(\"https://www.google.com/maps/dir//{lat},{lon}\"; \"Mapa\")"
#         print(output)
#         print(output, file=excel_file)

#         time.sleep(1)
