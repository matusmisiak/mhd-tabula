function openTab(id) {
    let tabButtonIds = {
        "fav-view": "fav-tab",
        "search-view": "search-tab",
        "near-view": "near-tab"
    }
    let views = document.getElementsByClassName('view')
    let tabs = document.getElementsByClassName('tab')
    for (let view of views) {
        view.style = 'display: none'
    }
    for (let tab of tabs) {
        tab.classList.remove('active-tab')
    }
    document.getElementById(id).style = 'display: block'
    document.getElementById(tabButtonIds[id]).classList.add('active-tab')
    navigator.geolocation.clearWatch(geoWatchId)
}

function addStop(stopList, stopName, stopId, labelText='') {
    let stopItem = document.createElement('li')
    let stopContainer = document.createElement('div')
    stopContainer.classList.add('stop-container')
    let busStop = document.createElement('a')
    busStop.innerHTML = stopName
    busStop.href = `https://imhd.sk/ba/online-zastavkova-tabula?st=${stopId}`
    if (labelText) {
        label = document.createElement('span')
        label.classList.add('dist-label')
        label.textContent = labelText
    }
    let starIcon = document.createElement('span')
    starIcon.classList.add('fa')
    starIcon.classList.add('fa-star-o')
    starIcon.classList.add('star')
    starIcon.classList.add('s'+stopId)
    starIcon.onclick = () => {updateFavs(stopId)}
    stopContainer.appendChild(busStop)
    if (labelText) stopContainer.appendChild(label)
    stopContainer.appendChild(starIcon)
    stopItem.appendChild(stopContainer)
    stopList.appendChild(stopItem)
}

function searchStops() {
    let results = fuse.search(searchInput.value)
    resultList.textContent = ''
    if (searchInput.value.length === 0) {
        results = stopObjects.map(function(r) {
            return {item: r}
        })
    }
    for (let result of results) {
        let stopId = result.item.id
        let stopName = result.item.name
        addStop(resultList, stopName, stopId)
    }
    loadFavs()
}

function updateFavs(stopId) {
    let favs = JSON.parse(localStorage.getItem('favourites'))
    if (!favs) {
        favs = []
    }
    if (favs.includes(stopId)) {
        favs.splice(favs.indexOf(stopId), 1)
    } else {
        favs.push(stopId)
    }
    localStorage.setItem('favourites', JSON.stringify(favs))
    loadFavs()
}

function loadFavs() {
    favList.textContent = ''
    let allstars = document.getElementsByClassName('star')
    changeStars(allstars, false)
    let favs = JSON.parse(localStorage.getItem('favourites'))
    if (!favs) {
        favs = []
    }
    for (let fav of favs) {
        addStop(favList, stops[fav]['name'], fav)
        starIcons = document.getElementsByClassName('s'+fav)
        changeStars(starIcons, true)
    }
}

function changeStars(starIcons, starred) {
    for (let starIcon of starIcons) {
        if (starred) {
            starIcon.classList.remove('fa-star-o')
            starIcon.classList.add('fa-star')
        } else {
            starIcon.classList.remove('fa-star')
            starIcon.classList.add('fa-star-o')
        }
    }
}

function rad(x) {
    return x * Math.PI / 180
}

function distance(lon1, lat1, lon2, lat2) {
    let R = 6371000 // Radius of the earth in m
    let dLat = rad(lat2-lat1)
    let dLon = rad(lon2-lon1)
    let a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(rad(lat1)) * Math.cos(rad(lat2)) *
            Math.sin(dLon/2) * Math.sin(dLon/2)
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))
    return R * c // Distance in m
}

function distanceFromHere(pos, stop) {
    return Math.min(...stop.coords.map(c => {
        return distance(pos.coords.longitude, pos.coords.latitude, c.lon, c.lat)
    }))
}

function humanizeDistance(dist) {
    if (dist < 1000) {
        return Math.round(dist) + ' m'
    } else {
        return (dist / 1000).toFixed(1) + ' km'
    }
}

function humanizeTimeDelta(timeDelta) {
    timeDelta = Math.round(timeDelta / 1000)
    let result = ''
    let hours = Math.floor(timeDelta / 3600)
    if (hours > 0) result += hours + ' h '
    let minutes = Math.floor(timeDelta / 60) % 60
    if (minutes > 0) result += minutes + ' m '
    let seconds = timeDelta % 60
    if (seconds > 0 || !result) result += seconds + ' s'
    return result.trim()
}

function updateGpsStatus() {
    if (lastGpsTime) {
        gpsLast.textContent = humanizeTimeDelta((Date.now() - lastGpsTime))
    }
}

function switchStatus(hasData) {
    document.getElementById('loading-gps').style.display = hasData ? 'none' : 'block'
    document.getElementById('status-gps').style.display = hasData ? 'block' : 'none'
}

function startNearest() {
    switchStatus(false)
    geoWatchId = navigator.geolocation.watchPosition(pos => {
        lastGpsTime = pos.timestamp
        switchStatus(true)
        updateGpsStatus()
        gpsAcuracy.textContent = humanizeDistance(pos.coords.accuracy)
        let results = nearStopObjects.sort((a, b) => {
            a_dist = distanceFromHere(pos, a)
            b_dist = distanceFromHere(pos, b)
            if (a_dist < b_dist) { return -1 }
            if (a_dist > b_dist) { return 1 }
            return 0
        }).slice(0, NEAREST_COUNT)
        nearList.textContent = ''
        for (let result of results) {
            let d = distanceFromHere(pos, result)
            let dist = humanizeDistance(d)
            addStop(nearList, ('text' in result && d < 10) ? `${result.name}: <pre>${result.text}</pre>` : result.name, result.id, dist)
        }
        loadFavs()
    }, () => {}, {maximumAge: Infinity})
}

const NEAREST_COUNT = 20

const searchInput = document.getElementById('search-input')
const resultList = document.querySelector('#search-view ul')
const favList = document.querySelector('#fav-view ul')
const nearList = document.querySelector('#near-view ul')
const gpsAcuracy = document.getElementById('gps-accuracy')
const gpsLast = document.getElementById('gps-last')

let geoWatchId = null
let lastGpsTime = null

let stopObjects = Object.keys(stops).map(function(key) {
    return {id: key, ...stops[key]}
}).sort(function(a, b) {
    if(a.name.toLowerCase() < b.name.toLowerCase()) { return -1 }
    if(a.name.toLowerCase() > b.name.toLowerCase()) { return 1 }
    return 0
})
let nearStopObjects = []
for (let stop of stopObjects) {
    if (stop.coords) {
        nearStopObjects.push(stop)
    }
}

let fuse = new Fuse(stopObjects, {keys: ['name']})

setInterval(updateGpsStatus, 1000)

// Remove invalid favs
let favs = JSON.parse(localStorage.getItem('favourites'))
if (!favs) favs = []
for (let fav of favs) {
    if (!stops[fav]) {
        favs.splice(favs.indexOf(fav), 1)
    }
}
localStorage.setItem('favourites', JSON.stringify(favs))

loadFavs()
// if (favs && favs.length > 0) {
//     openTab('fav-view')
// } else {
//     openTab('search-view')
//     searchStops()
// }

openTab('near-view')
startNearest()
